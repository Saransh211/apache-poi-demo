package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import Utility.WaitClass;
import junit.framework.Assert;

public class elementsPage 
{
	@FindBy(how=How.XPATH, using="//div[text()='Elements'][@class='header-text']")
	static WebElement elementTab;
	
	@FindBy(how=How.XPATH, using="//span[text()='Text Box']")
	public static WebElement textBoxTab;
	
	public static void clickElementstab()
	{
		WaitClass.waitANDclick(elementTab);
	}
	
	@SuppressWarnings("deprecation")
	public static void clicktextBoxTab()
	{
		WaitClass.waitANDclick(textBoxTab);
		Assert.assertEquals("https://demoqa.com/text-box", initBrowser.driver.getCurrentUrl());
	}
}
