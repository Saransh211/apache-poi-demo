package PageObjects;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Utility.WaitClass;
import Utility.WebElementsRepo;

import java.io.*;

public class initBrowser
{
	public static WebDriver driver;
	public static void triggerBrowser()
	{
	//	System.setProperty("WebDriver.chrome.driver", "C:\\drivers\\chromedriver_87.exe");
		driver = new ChromeDriver();
	}
	
	public static void openURL() throws InterruptedException
	{
		driver.get("https://demoqa.com/");	
		driver.manage().window().maximize();
	}

}
