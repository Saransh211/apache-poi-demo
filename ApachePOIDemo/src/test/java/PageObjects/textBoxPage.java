package PageObjects;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.WaitClass;
import junit.framework.Assert;

public class textBoxPage 
{
	@FindBy(how=How.XPATH, using="//input[@id='userName']")
	public static WebElement fullNameBox;
	@FindBy(how=How.XPATH, using="//input[@id='userEmail']")
	public static WebElement emailBox;
	@FindBy(how=How.XPATH, using="//textarea[@id='currentAddress']")
	public static WebElement addresstextArea;
	@FindBy(how=How.XPATH, using="//textarea[@id='permanentAddress']")
	public static WebElement permanenttextArea;
	@FindBy(how=How.XPATH, using="//button[@id='submit']")
	public static WebElement submitButton;
	@FindBy(how=How.XPATH, using="//p[@id='name']")
	public static WebElement nameP;
	@FindBy(how=How.XPATH, using="//p[@id='email']")
	public static WebElement emailP;
	@FindBy(how=How.XPATH, using="//p[@id='currentAddress']")
	public static WebElement currentAddressP;
	@FindBy(how=How.XPATH, using="//p[@id='permanentAddress']")
	public static WebElement permanentAddressP;
	static JavascriptExecutor js = (JavascriptExecutor) initBrowser.driver;  
	static ArrayList<Object> list = new ArrayList<Object>();
	public static void enterDetails() throws IOException
	{
		String excelFilePath = "DataSheet.xlsx";
		FileInputStream inputStream;
		inputStream = new FileInputStream(new File(excelFilePath));
		XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();
		while (iterator.hasNext()) 
		{
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while (cellIterator.hasNext()) 
			{
				Cell cell = cellIterator.next();
				switch (cell.getCellType()) 
				{
				case STRING:
					list.add(cell.getStringCellValue());
					break;
				case BOOLEAN:
					list.add(cell.getBooleanCellValue());
					break;
				case NUMERIC:
					list.add(cell.getNumericCellValue());
					break;
				}
			}
		}
		workbook.close();
		inputStream.close();
		WaitClass.waitfor(fullNameBox);
	
		js.executeScript("arguments[0].value='"+list.get(0)+"';", fullNameBox);
		js.executeScript("arguments[0].value='"+list.get(1)+"';", emailBox);
		js.executeScript("arguments[0].value='"+list.get(2)+"';", addresstextArea);
		js.executeScript("arguments[0].value='"+list.get(3)+"';", permanenttextArea);
		js.executeScript("arguments[0].click();", submitButton);
	}  
	
	public static void validateDetails()
	{
		js.executeScript("arguments[0].scrollIntoView(true);", nameP);
//		System.out.println(nameP.getText());
		Assert.assertEquals(nameP.getText().substring(5), list.get(0));
		Assert.assertEquals(emailP.getText().substring(6), list.get(1));
		Assert.assertEquals(currentAddressP.getText().substring(17), list.get(2));
		Assert.assertEquals(permanentAddressP.getText().substring(20), list.get(3));
	}
}
