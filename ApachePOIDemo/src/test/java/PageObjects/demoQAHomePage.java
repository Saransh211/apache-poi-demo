package PageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.WaitClass;
import Utility.WebElementsRepo;
import junit.framework.Assert;

public class demoQAHomePage 
{
	@FindBy(how=How.XPATH, using="//h5[text()='Elements']")
	public static WebElement demoqaHomePageElements;
	@SuppressWarnings("deprecation")
	public static void clickdemoqaHomePageElements() throws InterruptedException
	{
		WaitClass.waitANDclick(demoqaHomePageElements);
		Assert.assertEquals("https://demoqa.com/elements", initBrowser.driver.getCurrentUrl());
	}
}
