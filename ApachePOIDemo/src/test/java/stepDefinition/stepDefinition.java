package stepDefinition;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import PageObjects.demoQAHomePage;
import PageObjects.elementsPage;
import PageObjects.initBrowser;
import PageObjects.textBoxPage;
import Utility.WebElementsRepo;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class stepDefinition 
{	
	//Background stuff
	@When("User trigger the browser and enter URL")
	public void user_trigger_the_browser_and_enter_url() throws IOException 
	{
		
		initBrowser.triggerBrowser();	
	}

	@Then("Website will be displayed")
	public void website_will_be_displayed() throws InterruptedException 
	{	
		initBrowser.openURL();
	}
	//-----------------------

	//Scenario: Check whether Text Box Functionality link is working
	@Given("User navigate to the Text Box page")
	public void user_navigate_to_the_text_box_page() throws InterruptedException 
	{
		PageFactory.initElements(initBrowser.driver, demoQAHomePage.class);	
		PageFactory.initElements(initBrowser.driver, elementsPage.class);	
		demoQAHomePage.clickdemoqaHomePageElements();
		elementsPage.clickElementstab();
		elementsPage.clicktextBoxTab();
	}

	@When("User details are entered and Submit")
	public void user_details_are_entered_and_submit() throws IOException 
	{
		PageFactory.initElements(initBrowser.driver, textBoxPage.class);	
		textBoxPage.enterDetails();
	}	
	
	@When("System should show all the value entered correctly")
	public void system_should_show_all_the_value_entered_correctly() throws IOException 
	{
		textBoxPage.validateDetails();
	}
}
