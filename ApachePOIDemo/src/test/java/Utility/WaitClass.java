package Utility;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import PageObjects.initBrowser;
public class WaitClass 
{
	@SuppressWarnings("deprecation")
	static Wait<WebDriver> wait = new FluentWait<WebDriver>(initBrowser.driver)
			.withTimeout(60, TimeUnit.SECONDS)
			.pollingEvery(1, TimeUnit.SECONDS)
			.ignoring(Exception.class);

		public static void waitANDclick(WebElement locator)
		{
			//wait.until(ExpectedConditions.elementToBeClickable(locator));	
			JavascriptExecutor js =  (JavascriptExecutor)initBrowser.driver;
			js.executeScript("arguments[0].click()",locator);
		}
		
		public static void waitfor(WebElement locator)
		{
			wait.until(ExpectedConditions.elementToBeClickable(locator));	
		}
}

