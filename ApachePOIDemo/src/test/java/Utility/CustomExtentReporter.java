//package Utility;
//
//import java.io.File;
//import java.io.IOException;
//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//import io.cucumber.java.Scenario;
//
//public class CustomExtentReporter 
//{
//	private ExtentHtmlReporter extentHtmlReporter;
//	private ExtentReports extentReports;
//
//	public CustomExtentReporter(String reportLocation)
//	{
//		extentHtmlReporter = new ExtentHtmlReporter(new File(reportLocation));
//		extentReports = new ExtentReports();
//		extentReports.attachReporter(extentHtmlReporter);
//		System.out.println("Generated report");
//	}
//
//	public void createTest(Scenario scenario, String sceenShotFile) throws IOException
//	{
//		if(scenario!=null)
//		{
//			String testName = getScenarioTitle(scenario);
//			System.out.println(testName);
//			System.out.println(scenario.getStatus());
//			switch(scenario.getStatus())
//			{
//				case PASSED:
//					extentReports.createTest(testName).pass("Passed");
//				case FAILED:
//					extentReports.createTest(testName).fail("Failed").addScreenCaptureFromPath(getScreenShotLocation(sceenShotFile));
//				default:
//					extentReports.createTest(testName).skip("Skipped");
//			}
//		}
//	}
//	public void writeToReport()
//	{
//		if(extentReports != null)
//		{
//			extentReports.flush();
//		}
//	}
//	private String getScreenShotLocation(String aLocation)
//	{
//		return "file:///"+aLocation.replaceAll("\\", "//");
//	}
//	private String getScenarioTitle(Scenario scenario)
//	{
//		return scenario.getName().replace(" ", "");
//	}
//}
//
