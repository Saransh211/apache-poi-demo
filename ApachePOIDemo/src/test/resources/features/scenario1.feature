#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Test Login page of ToolsQA
		
	Background: Browser should be triggered
		When User trigger the browser and enter URL
		Then Website will be displayed

@Tag1
	Scenario: Check whether Text Box Functionality link is working
	Given User navigate to the Text Box page
	When User details are entered and Submit
	Then System should show all the value entered correctly

